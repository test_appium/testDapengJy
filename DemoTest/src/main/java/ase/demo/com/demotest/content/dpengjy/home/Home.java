package ase.demo.com.demotest.content.dpengjy.home;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ase.demo.com.demotest.content.dpengjy.login.Login;
import ase.demo.com.demotest.content.dpengjy.login.LoginOperate;
import ase.demo.com.demotest.pageobjbase.InitAppium;
import ase.demo.com.demotest.pageobjbase.util.ExcelDataProvider;
import io.appium.java_client.android.AndroidElement;

public class Home extends InitAppium {
    private HomeOperate homeOperate;


    @BeforeClass
    public void initDriver() {
        Assert.assertNotNull(driver);
        homeOperate = new HomeOperate(driver);
    }


    /**
     * 课程
     */
    @Test()
    public void selectRadiobutton1() {
        boolean tag = homeOperate.selectBottomTab(1);
        if (!tag) {
            print("selectRadiobutton1 执行异常");
        }
    }

    /**
     * 首页-未批改
     */
    @Test(priority = 1)
    public void selectRadiobutton0() {
        boolean tag = homeOperate.selectBottomTab(0);
        if (!tag) {
            print("selectRadiobutton1 执行异常");
        }
        String paht = homeOperate.screenshots();
        print("截图了·············" + paht);
        homeOperate.sleepTow();
        //自动滚动查询打分
        //autoScrennFindDafen();
        scrollView();

        //倒叙
        if (homeOperate.findById("clickIvHomeDece").isDisplayed())
            homeOperate.clickView(homeOperate.findById("clickIvHomeDece"));

        homeOperate.sleepTree();
        //评价->取消
        List<WebElement> pinglunList = homeOperate.findListByIds("pinglun");
        if (pinglunList != null && pinglunList.size() != 0 && pinglunList.get(0) != null) {
            homeOperate.clickView((AndroidElement) pinglunList.get(0));
            AndroidElement cancel = homeOperate.findById("cancel");
            homeOperate.clickView(cancel);
        }
        //推荐
        List<WebElement> recommList = homeOperate.findRecommIds();
        if (recommList != null && recommList.size() != 0 && recommList.get(0) != null) {
            homeOperate.clickView((AndroidElement) recommList.get(0));
        }
        //作业要求
        List<WebElement> zuoyeyaoqiuList = homeOperate.findListByIds("zuoyeyaoqiu");
        if (zuoyeyaoqiuList != null && zuoyeyaoqiuList.size() != 0 && zuoyeyaoqiuList.get(0) != null) {
            homeOperate.clickView((AndroidElement) zuoyeyaoqiuList.get(0));
            //关闭作业要求
            AndroidElement cancel = homeOperate.findById("closeimg2");
            homeOperate.clickView(cancel);
        }
        //全屏图片
        clickImg2Screen();
        //结束全屏图
        clickImg2Small();
        homeOperate.waitAuto(5);
        //搜索
        searchText();
        homeOperate.swipeToDown();


        homeOperate.sleepFive();
    }


    /**
     * 首页-已批改
     */
    @Test(priority = 2)
    public void selectRadioButton0Already() {
        //已批改-->打分-->推荐
        homeOperate.selectTopTab(1);
        //打分
        List<WebElement> idList = homeOperate.findDafenIds();
        if (idList != null && idList.size() > 0) {
            homeOperate.clickView((AndroidElement) idList.get(0));
        }

        //推荐
        List<WebElement> recommList = homeOperate.findRecommIds();
        if (recommList != null && recommList.size() > 0) {
            homeOperate.waitAuto(2);
            homeOperate.clickView((AndroidElement) recommList.get(0));
            homeOperate.waitAuto(3);
            homeOperate.clickView((AndroidElement) recommList.get(0));
            homeOperate.waitAuto(5);
        }

        //评论删除
        List<WebElement> delList = homeOperate.findDelConetntIds();
        AndroidElement delContent = (AndroidElement) delList.get(0);
        if (delContent.isDisplayed()) {
            homeOperate.clickView(delContent);
            homeOperate.waitAuto(5);
            //弹窗取消
            //任意位置取消
            homeOperate.press(200, 300);
            homeOperate.sleep(500);
            //按钮取消
            homeOperate.clickView(delContent);
            homeOperate.waitAuto(2);
            homeOperate.clickView(homeOperate.findDelDiaglogCancelId());
            homeOperate.waitAuto(5);
        }
    }


    //===================================
    //搜索->删除->搜索
    private void searchText() {
        homeOperate.inputSearchText("测试输入框输入内容显示操作aass");
        homeOperate.delSearchText();
        homeOperate.inputSearchText("学员");
        homeOperate.inputKeyBorde();
    }

    //自动滚动找text=打分
    private void autoScrennFindDafen() {
        AndroidElement androidElement = homeOperate.autoScorllXYID("打分");
        if (androidElement != null && androidElement.isDisplayed()) {
            homeOperate.clickView(androidElement);
            //List<WebElement> scoreimgList = homeOperate.findListByIds("scoreimg");
            /* if (scoreimgList != null && scoreimgList.size() > 0) {
               if (!scoreimgList.get(0).isDisplayed()) {
                    homeOperate.clickView(androidElement);
                } else {
                    print("没有找到合适的继续找~~~~~1");
                    autoScrennFindDafen();
                }
            } else {
                print("没有找到合适的继续找~~~~~2");
                autoScrennFindDafen();
            }*/
        } else {
            print("没有找到合适的继续找~~~~~3");
            autoScrennFindDafen();
        }
    }

    /**
     * 滚动列表 直到出现打分按钮
     *
     * @param
     * @return
     */
    public String scrollView() {
        while (true) {
            homeOperate.swipeToUpSmall();
            List<WebElement> idList = homeOperate.findDafenIds();
            if (idList.size() != 0 && idList.get(0) != null) {
                homeOperate.clickView((AndroidElement) idList.get(0));
                homeOperate.sleepOne();
                List<WebElement> scoreimgList = homeOperate.findListByIds("scoreimg");
                if (scoreimgList == null || scoreimgList.size() == 0) {
                    homeOperate.press(200, 500);
                }
                print("找到 空间了·············");
                break;
            }
        }
       /* if (index < 12) {
            homeOperate.swipeToUpSmall();
            List<WebElement> idList = homeOperate.findDafenIds();
            if (idList.size() != 0 && idList.get(0) != null) {
                List<WebElement> scoreimgList = homeOperate.findListByIds("scoreimg");
                print("打分 等级：" + scoreimgList.size());
                if (scoreimgList == null || scoreimgList.size() == 0) {
                    if ((idList.get(0)).isDisplayed() && scoreimgList.size() == 0) {
                        homeOperate.clickView((AndroidElement) idList.get(0));
                        *//*homeOperate.sleepTow();
                        homeOperate.press(200, 500);*//*
                        return "###找到了打分的作业";
                    }
                }
            }
            index++;
            print("滚动吧页面：" + index);
            scrollView(index);
        } else {
            return "@@@未到了打分的作业";
        }*/
        return "";
    }


    //点击图片显示全屏图片
    private void clickImg2Screen() {
        List<WebElement> idList = homeOperate.findListImgIds();
        if (idList.size() != 0 && idList.get(0) != null) {
            homeOperate.clickView((AndroidElement) idList.get(0));
        }
    }

    //全屏图片结束
    private void clickImg2Small() {
        List<WebElement> idList = homeOperate.findListByIds("fImageView");
        if (idList.size() != 0 && idList.get(0) != null) {
            homeOperate.clickView((AndroidElement) idList.get(0));
        }
    }


}
