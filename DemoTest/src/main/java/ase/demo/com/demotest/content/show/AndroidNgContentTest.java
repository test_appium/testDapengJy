package ase.demo.com.demotest.content.show;

import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

//appium+testng
public class AndroidNgContentTest {
    @BeforeSuite
    public void testBeforeSuite() {
        //注解的方法将只运行一次，运行在此套件中的所有测试之前。
    }

    @AfterSuite
    public void testAfterSuite() {
        //注解的方法将只运行一次，运行在此套件中的所有测试之后
    }

    @BeforeClass
    public void testBeforeClass() {
        //注解的方法将只运行一次，运行在该class运行之前
    }

    @AfterClass
    public void testAfterClass() {
        //注解的方法将只运行一次，运行在该class之后
    }

    @AfterTest
    public void testAfterTest() {
        //class的标签之后被运行
    }

    @BeforeTest
    public void testBeforeTest() {
        //class的标签之前被运行
    }

    @BeforeGroups
    public void testBeforeGroups() {
        //组的列表，这种配置方法将之前运行。此方法是保证在运行属于任何这些组第一个测试方法，该方法被调用
    }

    @AfterGroups
    public void testAfterGroups() {
        //组的名单，这种配置方法后，将运行。此方法是保证运行后不久，最后的测试方法，该方法属于任何这些组被调用
    }

    @BeforeMethod
    public void testBeforeMethod() {
        //注解的方法将每个测试方法之前运行
    }

    @AfterMethod
    public void testAfterMethod() {
        //被注释的方法将被运行后，每个测试方法
    }

    @DataProvider
    public Object[][] testDataProvider() {
        //标志着一个方法，提供数据的一个测试方法。注解的方法必须返回一个Object[][]，
        // 其中每个对象[]的测试方法的参数列表中可以分配。该@Test 方法，希望从这个DataProvider的接收数据，
        // 需要使用一个dataProvider名称等于这个注解的名字
        return null;
    }

    @Factory
    public Object[] testFactory() {
        //作为一个工厂，返回TestNG的测试类的对象将被用于标记的方法。该方法必须返回Object[]
        return null;
    }

    /*@Listeners
    public void testListeners() {
        //定义一个测试类的监听器
    }*/

    @Parameters
    public void testParameters() {
        //将参数传递给@Test方法
    }

    @Test
    public void testTest() {
        //标记一个类或方法作为测试的一部分
    }

    //PS: 设置程序信息一般放在BeforeClass或者BeforeSuite里面，不要放BeforeMethod里，为什么呢，
    // 如果有时候不止一个测试方法，放BeforeMethod里面就会导致调用多次，就会报错了。还有你要并行测试的话，
    // 设置信息一定得放在BeforeClass，不然就会导致只初始化一次，就会只有一台手机测试 。
    // 所以，初始化放在BeforeClass是最安全的，（例如我第十篇文章并行那里有讲到）


}
