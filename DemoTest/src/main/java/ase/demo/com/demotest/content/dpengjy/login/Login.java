package ase.demo.com.demotest.content.dpengjy.login;


import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Map;

import ase.demo.com.demotest.base.Assertion;
import ase.demo.com.demotest.base.Builder;
import ase.demo.com.demotest.pageobjbase.InitAppium;
import ase.demo.com.demotest.pageobjbase.util.ExcelDataProvider;

//登录测试用例
public class Login extends InitAppium {
    private LoginOperate loginOperate;


    @Parameters({"udid", "port"})
    public Login(String udid, String port) {
        super(new Builder().setUdid(udid).setPort(port));
        print("设备:udid:" + udid + "--port:" + port);
    }


    @BeforeClass
    public void initDriver() {
        Assert.assertNotNull(driver);
        loginOperate = new LoginOperate(driver);
    }

    /**
     * 测试帐号对 密码不对情况
     */
   /* @Test()
    public void loginErrorUser() {
        boolean flag = loginOperate.login("13500000000", "123456a3");
        Assertion.verifyEquals(flag, true, "帐号对密码错误是否登录成功");
        print("帐号密码不对情况登录:" + flag);
    }*/


    /**
     * 测试帐号密码规格不对情况
     */
    /*@Test(priority = 1)
    public void loginErrorNum() {
        boolean flag = loginOperate.login("1319262asdfsddsasdfsdfsdfsdfsdfsdf4740", "dfgd#@$1234fgdsfgdsgdffds");
        Assertion.verifyEquals(flag, true, "帐号密码格式不对是否登录成功");
        print("帐号密码格式不对情况登录:" + flag);
    }*/


    /**
     * 测试帐号密码为中文情况
     */
    /* @Test(priority = 2)
   public void loginChinese() {
        boolean flag = loginOperate.login("帐号", "密码");
        Assertion.verifyEquals(flag, true, "帐号密码为中文是否登录成功");
        print("帐号密码为中文情况登录:" + flag);
    }*/


    /**
     * 测试帐号密码为空情况
     */
    /*@Test(priority = 3)
    public void loginEmpty() {
        boolean flag = loginOperate.login("", "");
        Assertion.verifyEquals(flag, true, "帐号密码为空是否登录成功");
        print("帐号密码为空情况登录:" + flag);
    }*/


    //通过DataProvider设置简单固定数据
    //通过DataProvider设置简单固定数据
    /*@DataProvider(name = "mDataProvider")
    public Object[][] mDataProvider() {
        return new Object[][]{{"战三", 2, "河北"}, {"找死", 12, "河南"}};
    }


    @Test(dataProvider = "mDataProvider")
    public void testDataProvide(String name, int age, String address) {
        print("参数----::name:" + name + "-" + age + "-" + address);
    }*/


    /**
     * 测试帐号密码正确情况
     */
    @Test(parameters = "lname", priority = 1)
    public void loginConfim(String parma) {
        print("接受到的参数----::parma:" + parma);
        boolean flag = loginOperate.login("17663724914", "123456a");
        loginOperate.waitAuto();
        print("帐号密码对的情况登录:" + flag);
        /* Assert.assertTrue(flag, "帐号密码对的情况登录");*/
    }


}
