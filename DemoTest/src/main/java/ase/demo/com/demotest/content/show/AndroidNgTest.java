package ase.demo.com.demotest.content.show;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeSuite;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

//appium+testng
public class AndroidNgTest {
    private AppiumDriver<AndroidElement> driver;
    private String sutrix = ".apk";
    //测试apk名称
    public String appFileName1 = "ContactManager";
    public String appFileName2 = "DaPengJy";

    //包名
    private String packageName1 = "com.example.android.contactmanager";
    private String packageName2 = "com.beijing.dapeng";

    //启动类
    private String activityName1 = ".ContactManager";
    private String activityName2 = ".view.activity.LoadingActivity";

    @Before
    public void setUp() throws MalformedURLException {
        File classPahtRoot = new File(System.getProperty("user.dir"));
        File appDir = new File(classPahtRoot, "/src/main/java/apps");
        File app = new File(appDir, appFileName2 + sutrix);//测试应用+扩展名

        DesiredCapabilities capabilites = new DesiredCapabilities();
        capabilites.setCapability("deviceName", "1605-A01");//手机名称(关于手机/关于系统-->手机型号)
        capabilites.setCapability("platformVersion", "6.0.1");//手机中的安卓版本

        capabilites.setCapability("app", app.getAbsolutePath());
        capabilites.setCapability("appPackage", packageName2);//应用包名
        capabilites.setCapability("appActivity", activityName2);//应用包名启动的Activity


        driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabilites);


        //遇到问题已经解决方式:
        //问题1：
        //appium连接模拟器找不到驱动
        //解决方法:连接真机测试
    }

    @After
    public void tearDown() throws Exception {
        //测试完毕，关闭driver，不关闭将会导致会话还存在，下次启动就会报错
        driver.quit();
    }

    @Test
    public void launchBrowser() {
    }

    /**
     * 要执行的的测试方法
     */
    @Test
    public void addContact() {
        //利用Xpath的方法寻找text值为Add Contact的控件
//        WebElement el = driver.findElement(By.xpath(".//*[@text='Add Contact']"));
        /*WebElement el = driver.findElement(By.id("R.id.loginname"));
        //点击这个控件
        el.click();*/
        //利用类名获取界面上所有的EditText
        List<AndroidElement> textFieldsList = driver.findElementsByClassName("android.widget.EditText");
        //第一个EditText输入内容Some Name
        textFieldsList.get(0).sendKeys("Some Name");
        //第三个EditText输入内容Some Name
        textFieldsList.get(2).sendKeys("Some@example.com");
        //在坐(100,500)滑动到(100,100) 时间为2毫秒
        driver.swipe(100, 500, 100, 100, 2);
        //用xpath的方式寻找到text值为Save的控件，然后点击
        driver.findElementByXPath(".//*[@text='Save']").click();
    }




}
