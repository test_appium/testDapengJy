package ase.demo.com.demotest.content.dataprovider.exceldataprovider;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Map;

import ase.demo.com.demotest.pageobjbase.util.ExcelDataProvider;

public class AppInitData {

    //通过DataProvider 迭代器设置读取电脑Excel数据
    //迭代器
    @DataProvider(name = "data")
    public Iterator<Object[]> dataForTestMethod(Method method) throws FileNotFoundException {
        String sheetName = "Sheet1";
        return (Iterator<Object[]>) new ExcelDataProvider(sheetName);
    }

    @Test(dataProvider = "data")
    public void interfaceTest(Map<String, String> map) {
        String caseName = map.get("用例名称");
        String xieyi = map.get("协议");
        System.out.println("caseName" + caseName + "xieyi" + xieyi);

    }

}
