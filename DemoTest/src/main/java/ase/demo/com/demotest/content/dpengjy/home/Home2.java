package ase.demo.com.demotest.content.dpengjy.home;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

import ase.demo.com.demotest.pageobjbase.InitAppium;
import io.appium.java_client.android.AndroidElement;

public class Home2 extends InitAppium {
    private HomeOperate homeOperate;


    @BeforeClass
    public void initDriver() {
        deviceName="OPPO";
        Assert.assertNotNull(driver);
        homeOperate = new HomeOperate(driver);
    }


    /**
     * 课程
     */
    @Test()
    public void selectRadiobutton1() {
        boolean tag = homeOperate.selectBottomTab(1);
        if (!tag) {
            print("selectRadiobutton1 执行异常");
        }
    }

    /**
     * 首页-未批改
     */
    @Test(priority = 1)
    public void selectRadiobutton0() {
        boolean tag = homeOperate.selectBottomTab(0);
        if (!tag) {
            print("selectRadiobutton1 执行异常");
        }

        //打分
        AndroidElement dafen = scrollView(null);
        homeOperate.clickView(dafen);
        homeOperate.sleepTree();

        //搜索
        searchText();
        homeOperate.swipeToDown();

        //
        homeOperate.sleepFive();
    }

    /**
     * 首页-已批改
     */
    @Test(priority = 2)
    public void selectRadioButton0Already() {
        //已批改-->打分-->推荐
        homeOperate.selectTopTab(1);
        //打分
        List<WebElement> idList = homeOperate.findDafenIds();
        homeOperate.clickView((AndroidElement) idList.get(0));

        //推荐
        List<WebElement> recommList = homeOperate.findRecommIds();
        homeOperate.waitAuto(2);
        homeOperate.clickView((AndroidElement) recommList.get(0));
        homeOperate.waitAuto(3);
        homeOperate.clickView((AndroidElement) recommList.get(0));
        homeOperate.waitAuto(5);

        //评论删除
        List<WebElement> delList = homeOperate.findDelConetntIds();
        AndroidElement delContent = (AndroidElement) delList.get(0);
        if (delContent.isDisplayed()) {
            homeOperate.clickView(delContent);
            homeOperate.waitAuto(5);
            //弹窗取消
            //任意位置取消
            homeOperate.press(200, 300);
            homeOperate.sleep(500);
            //按钮取消
            homeOperate.clickView(delContent);
            homeOperate.waitAuto(2);
            homeOperate.clickView(homeOperate.findDelDiaglogCancelId());
            homeOperate.waitAuto(5);
        }
    }


    //===================================
    //搜索->删除->搜索
    private void searchText() {
        homeOperate.inputSearchText("测试输入框输入内容显示操作aass");
        homeOperate.delSearchText();
        homeOperate.inputSearchText("学员");
        homeOperate.inputKeyBorde();
    }


    /**
     * 滚动列表 直到出现打分按钮
     *
     * @param list
     * @return
     */
    public AndroidElement scrollView(List<WebElement> list) {
        if (list == null || list.size() == 0) {
            List<WebElement> idList = homeOperate.findDafenIds();
            if (idList.size() != 0 && idList.get(0) != null) {
                print("scrollView-=====包含打分按钮:" + idList.size()+"--"+idList.get(0));
                homeOperate.clickView((AndroidElement) idList.get(0));
                return (AndroidElement) idList.get(0);
            }
            print("scrollView-=====:" + idList.size());
            homeOperate.swipeToUpSmall();
            scrollView(null);
        }
        return null;
    }


}
