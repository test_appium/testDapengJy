package ase.demo.com.demotest.content.dpengjy.home;

import org.openqa.selenium.WebElement;

import java.util.List;

import ase.demo.com.demotest.pageobjbase.PageAppium;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class HomePage extends PageAppium {

    static String NSEARCHID = "search_edit";  //搜索框
    static String NDELSEARCHID = "cancel_search";//删除搜索
    static String IMG = "ivImge";//图片
    static String DAFEN = "dafen";//打分
    static String AUDIO = "yuyin";//语音
    static String CONMENT = "pinglun";//评论
    static String RECOMM = "recomendimg";//推荐
    static String NOTIC = "tvNotic";//提醒
    static String DELCONETNT = "pl_del_1_temp";//删除评论
    static String DELDIALOGCANCEL = "tvCancel";//删除评论弹窗-取消
    static String DELDIALOGCOMFIER = "tvTrue";//删除评论弹窗-确定

    String[] bottomRiado = {"radiobutton0", "radiobutton1", "radiobutton2", "radiobutton3"};
    String[] topRiado = {"nopg_lay", "pg_lay"};

    public HomePage(AndroidDriver androidDriver) {
        super(androidDriver);
    }


    //搜索框
    public AndroidElement findSearchById() {
        return findById(NSEARCHID);
    }

    //取消搜索
    public AndroidElement findDelSearchById() {
        return findById(NDELSEARCHID);
    }

    public AndroidElement findDafenhById() {
        return findById(DAFEN);
    }

    //============打分===语音====评论=====推荐====提醒=====删除==============================================
    public List<WebElement> findDafenIds() {
        return findByIds(DAFEN);
    }

    public List<WebElement> findAudioIds() {
        return findByIds(AUDIO);
    }

    public List<WebElement> findContentIds() {
        return findByIds(CONMENT);
    }

    public List<WebElement> findRecommIds() {
        return findByIds(RECOMM);
    }

    public List<WebElement> findNoticIds() {
        return findByIds(NOTIC);
    }

    public List<WebElement> findDelConetntIds() {
        return findByIds(DELCONETNT);
    }


    public AndroidElement selectTopTab(int index) {
        return findById(topRiado[index]);
    }

    public AndroidElement selectRadiobutton1(int index) {
        return findById(bottomRiado[index]);
    }

    public String getnSearchId() {
        return NSEARCHID;
    }


    //删除提示窗  取消按钮
    public AndroidElement findDelDiaglogCancelId() {
        return findById(DELDIALOGCANCEL);
    }

    //删除提示窗  确定按钮
    public AndroidElement findDelDiaglogDetermineId() {
        return findById(DELDIALOGCOMFIER);
    }

    public List<WebElement> findListImg() {
        return findByIds(IMG);
    }

    public List<WebElement> findListByIds(String ids) {
        return findByIds(ids);
    }
}
