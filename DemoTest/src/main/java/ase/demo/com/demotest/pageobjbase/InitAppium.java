package ase.demo.com.demotest.pageobjbase;

import org.apache.http.util.TextUtils;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import ase.demo.com.demotest.base.Builder;

/**
 * 测试用例的父类
 * Created by LITP on 2016/9/7.
 */
@Listeners({ase.demo.com.demotest.base.AssertionListener.class})
public class InitAppium {

    static String udid = "127.0.0.1:62025", port = "4723";
    static String[] androidVer = {"4.4.2", "5.1.1", "6.0.1", "7.1.2"};
    //调试设备名字
    public static String deviceName = "1605-A01";//真机
    public static String deviceNameMu = "1605-A01";//真机
    //    public static String deviceName = "oppo";
    //调试设备系统版本
    public static String platformVersion = androidVer[1];
    //app路径
    public static String appPath = System.getProperty("user.dir")
            + "/src/main/java/apps/DaPengJy.apk";

    //包名
    public static String appPackage = "com.beijing.dapeng";

    //是否需要重新安装
    public static String noReset = "True";

    //是否不重新签名
    public static String noSign = "True";

    //是否使用unicode输入法，真是支持中文
    public static String unicodeKeyboard = "True";

    //是否祸福默认呢输入法
    public static String resetKeyboard = "True";

    //要启动的Activity
    public static String appActivity = appPackage + ".view.activity.HomeActivity";
//    public static String appActivity = "";

    public static AndroidDriver<AndroidElement> driver = null;


    //构造方法
    public InitAppium() {
        this(new Builder());
    }

    public InitAppium(Builder builder) {

        appActivity = builder.appActivity;
        appPackage = builder.appPackage;
        appPath = builder.appPath;
        deviceName = builder.deviceName;
        noReset = builder.noReset;
        noSign = builder.noSign;
        unicodeKeyboard = builder.unicodeKeyboard;
        resetKeyboard = builder.resetKeyboard;
        if (!TextUtils.isEmpty(builder.getUdid())) {
            udid = builder.getUdid();
        }
        if (!TextUtils.isEmpty(builder.getPort())) {
            port = builder.getPort();
        }
    }

    /**
     * appium启动参数
     *
     * @throws MalformedURLException
     */
    @BeforeSuite
//    @BeforeClass
    public void beforeSuite() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformVersion", platformVersion);
        capabilities.setCapability("app", new File(appPath).getAbsolutePath());
        capabilities.setCapability("appPackage", appPackage);
        //支持中文
        capabilities.setCapability("unicodeKeyboard", unicodeKeyboard);
        //运行完毕之后，变回系统的输入法
        capabilities.setCapability("resetKeyboard", resetKeyboard);
        //不重复安装
        capabilities.setCapability("noReset", noReset);
        //不重新签名
        capabilities.setCapability("noSign", noSign);
        //打开的activity
        if (!TextUtils.isEmpty(appActivity)) {
            capabilities.setCapability("appActivity", appActivity);
        }

        //并发时
        if (!TextUtils.isEmpty(udid)) {
            capabilities.setCapability("udid", udid);
            capabilities.setCapability("deviceName", udid);
            print("并发的:" + udid + "----" + port);
        }else{
            capabilities.setCapability("deviceName", deviceName);
        }

        //启动Driver
        driver = new AndroidDriver<>(new URL("http://127.0.0.1:" + port + "/wd/hub"), capabilities);
        int height = driver.manage().window().getSize().height;
        int width = driver.manage().window().getSize().width;

        print("手机屏幕宽高:" + width + "x" + height);

    }


    @AfterTest
    public void afterTest() {
        driver.quit();
    }

    @AfterClass
    public void afterClass() {
        //每一个用例完毕结束这次测试
        //driver.quit();
    }

    /**
     * 打印字符
     *
     * @param str 要打印的字符
     */
    public <T> void print(T str) {
        if (!TextUtils.isEmpty(String.valueOf(str))) {
            System.out.println(str);
        } else {
            System.out.println("输出了空字符");
        }
    }


}
