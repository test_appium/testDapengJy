package ase.demo.com.demotest.content.dpengjy.home;

import org.openqa.selenium.WebElement;

import java.util.List;

import ase.demo.com.demotest.pageobjbase.OperateAppium;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class HomeOperate extends OperateAppium {
    AndroidDriver driver;
    HomePage homePage;

    public HomeOperate(AndroidDriver androidDriver) {
        super(androidDriver);
        homePage = new HomePage(androidDriver);
        this.driver = androidDriver;
    }


    /**
     * 填入搜索内容
     *
     * @param searchText
     */
    public void inputSearchText(String searchText) {
        clickView(homePage.findSearchById());
        inputManyTextById(searchText, homePage.getnSearchId());
        sleep(500);
    }

    /**
     * 取消搜索
     */
    public void delSearchText() {
        clickView(homePage.findDelSearchById());
        sleepTow();
    }

    public void swipeToDown() {
        swipeToDown(500);
        sleepTow();
    }

    /**
     * 睡眠等待2秒
     */
    public void sleepTow() {
        sleep(2000);
    }

    /**
     * 睡眠等待3秒
     */
    public void sleepOne() {
        sleep(1000);
    }

    /**
     * 睡眠等待3秒
     */
    public void sleepTree() {
        sleep(3000);
    }

    /**
     * 睡眠等待5秒
     */
    public void sleepFive() {
        sleep(5000);
    }


    /**
     * 切换顶部tab 0-2
     *
     * @param index
     * @return
     */
    public boolean selectTopTab(int index) {
        if (index < 0 || index > 2) {
            print("页面的顶部按钮没有找到");
            return false;
        }
        clickView(homePage.selectTopTab(index));
        return true;
    }

    /**
     * 切换tab
     *
     * @param index 需要切换的位置0-3
     * @return
     */
    public boolean selectBottomTab(int index) {
        if (index < 0 || index > 4) {
            print("首页选中的底部按钮没有找到");
            return false;
        }
        clickView(homePage.selectRadiobutton1(index));
        return true;
    }

    public void inputKeyBorde() {
        inputKeyBord(homePage.findSearchById());
        sleepTow();
    }


    public void findDafenhByName() {
        //clickView(homePage.findDafenhById());
        List<WebElement> webElements = homePage.findByClassNames("打分");
        print("通过Name查询打分按钮-=====:" + webElements.size());
        if (webElements.size() > 0) {
            for (int i = 0; i < webElements.size(); i++) {
                AndroidElement androidElement = (AndroidElement) webElements.get(i);
                if (i == 2) {
                    clickView(androidElement);
                }
            }
        }
    }

    //============打分===语音====评论=====推荐====提醒===================================================
    public List<WebElement> findDafenIds() {
        return homePage.findDafenIds();
    }

    public List<WebElement> findAudioIds() {
        return homePage.findAudioIds();
    }

    public List<WebElement> findContentIds() {
        return homePage.findContentIds();
    }

    public List<WebElement> findRecommIds() {
        return homePage.findRecommIds();
    }

    public List<WebElement> findNoticIds() {
        return homePage.findNoticIds();
    }

    public List<WebElement> findDelConetntIds() {
        return homePage.findDelConetntIds();
    }

    //删除提示窗  取消按钮
    public AndroidElement findDelDiaglogCancelId() {
        return homePage.findDelDiaglogCancelId();
    }

    //删除提示窗  确定按钮
    public AndroidElement findDelDiaglogDetermineId() {
        return homePage.findDelDiaglogDetermineId();
    }

    public List<WebElement> findListImgIds() {
        return homePage.findListImg();
    }

    public List<WebElement> findListByIds(String ids) {
        return homePage.findListByIds(ids);
    }

    public AndroidElement findById(String cancel) {
      return   homePage.findById(cancel);
    }
}
