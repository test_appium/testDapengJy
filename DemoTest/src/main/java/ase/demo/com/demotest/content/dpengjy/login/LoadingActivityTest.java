package ase.demo.com.demotest.content.dpengjy.login;

import org.testng.Assert;
import org.testng.annotations.Test;

import ase.demo.com.demotest.base.BaseAppium;
import ase.demo.com.demotest.base.Builder;

public class LoadingActivityTest extends BaseAppium {

    public LoadingActivityTest() {
        super(new Builder()
                .setAppActivity(".view.activity.LoadingActivity")
                .setAppPath("DaPengJy.apk"));
    }

    @Test(priority = 0)
    public void startTest() throws InterruptedException {
        sleep(1000);
        waitAuto();
        //输入帐号密码
        inputManyText("讲师35","123456a");
        //点击登录
        clickView(findById("next"),"登录");
        sleep(3);
        //点击一下左下角，避免第一次打开的指导
        press(10,getScreenHeight()-10);
        //首页是否存在，不存在就会抛出错误
        Assert.assertTrue(isNameElementExist("首页"),"判断是否到了首页");
    }
}
