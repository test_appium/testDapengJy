package ase.demo.com.demotest.content.dataprovider.readexcel;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import ase.demo.com.demotest.pageobjbase.util.CaseHelper;
import ase.demo.com.demotest.pageobjbase.util.CaseInfo;
import ase.demo.com.demotest.pageobjbase.util.ReadExcel;

public class AppInitData2 {
//    protected String caseExcelPath = System.getProperty("user.dir") + "\\excel\\temp.xlsx";
    protected String caseExcelPath =  "F:\\user\\user2.xls";

    @DataProvider(name = "dataInfo")
    protected Object[][] dataInfo1() throws IOException {

        Object[][] myObj = null;
        List<Map<String, String>> list = ReadExcel.readXlsx(caseExcelPath);
        myObj = CaseHelper.getObjArrByList(list);
        return myObj;
    }

    @Test(dataProvider = "dataInfo")
    public void testByExcel_Body(CaseInfo c) throws IOException {
        ///获取用例说明
        System.out.println(c.getCaseDesc());
        ///获取用例需要的参数
        System.out.println(c.getCaseParam());
        //获取执行用例需要的前置条件
        System.out.println(c.getCasePreset());
    }

}
