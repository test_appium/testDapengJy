package ase.demo.com.demotest.content.dataprovider.exceldata;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.testng.Assert;
import org.testng.TestNG;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import ase.demo.com.demotest.pageobjbase.util.ExcelData;
import jxl.read.biff.BiffException;

public class CalculatorExcel {


    @DataProvider(name = "num")
    public Object[][] Numbers() throws BiffException, IOException {
        ExcelData e = new ExcelData("user3", "login");
        return e.getExcelData();
    }

    @Test(dataProvider = "num")
    public void testAdd(HashMap<String, String> data) {
        System.out.println(data.toString());
        String num1 = data.get("用例名称");
        String num2 =data.get("协议");
        String expectedResult = data.get("是否执行");
        System.out.println(num1+"=="+num2+"=="+expectedResult);

        Assert.assertEquals("y", expectedResult);
        if("y".equals(expectedResult)){
            TestNG testNG = new TestNG();
            List<String> suites = new ArrayList<String>();
            suites.add("home.xml");
            testNG.setTestSuites(suites);
            testNG.run();
        }
    }


}
