package ase.demo.com.appmain;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import ase.demo.com.appbean.reflection.RTestUserBean;

public class RConstructor {

    public static void getConstructor(String className) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        //通过反射
        Class mclass = Class.forName(className);
        Constructor ct = mclass.getConstructor(String.class, String.class);
        RTestUserBean ob = (RTestUserBean) ct.newInstance("北京", "姓名");
        //newInstance 执行构造方法

        Method getListenerInfo = mclass.getDeclaredMethod("getPrintLog", null);
        getListenerInfo.invoke(ob, null);
        //invoke 执行成员方法


        //通过类对象
        Constructor crr = RTestUserBean.class.getConstructor(null);
        Method getListenerInfos = RTestUserBean.class.getDeclaredMethod("getPrintLog", null);
        getListenerInfos.setAccessible(true);
        getListenerInfo.invoke(crr.newInstance(null), null);


    }

}
