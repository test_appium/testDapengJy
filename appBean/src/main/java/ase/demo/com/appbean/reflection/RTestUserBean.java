package ase.demo.com.appbean.reflection;

public class RTestUserBean {
    private String address;
    private String name;
    private String tel;
    private int age;

    public RTestUserBean() {
        System.out.print("---------RTestUserBean");
    }

    public RTestUserBean(String name, int age) {
        this.name = name;
        this.age = age;
        System.out.print("---------RTestUserBean name age");
    }

    public RTestUserBean(String address, String name) {
        this.address = address;
        this.name = name;
        System.out.print("---------RTestUserBean address name " + name + "--" + address);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void getPrintLog() {
        System.out.print("getPrintLog--------------");
    }
}
