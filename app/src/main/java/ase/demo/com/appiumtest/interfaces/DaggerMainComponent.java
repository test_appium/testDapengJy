package ase.demo.com.appiumtest.interfaces;

import javax.inject.Singleton;

import ase.demo.com.appiumtest.model.CheckDataModel;
import ase.demo.com.appiumtest.model.DaggerUserModel;
import ase.demo.com.appiumtest.ui.activity.DaggerMainActivity;
import dagger.Component;

@Singleton
@Component(modules = {DaggerUserModel.class,CheckDataModel.class})
public interface DaggerMainComponent {
    void inject(DaggerMainActivity activity);
}
