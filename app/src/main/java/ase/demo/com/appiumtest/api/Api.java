package ase.demo.com.appiumtest.api;

public class Api {
    //static String baseUrlZT = "test.dapengjiaoyu.com/";//测试服务器上传base64文件时路径

    //======================================①数据请求路径======测试+正式========================================================
    //请求路径
    static String baseUrlZ = "tuapi.dapengjiaoyu.com/";//测试
//    private static String baseUrlZ = "api.dapengjiaoyu.com/";//正式


    //请求链接
    public static String baseUrl = "https://" + baseUrlZ + "api/";//测试
//    public static String baseUrl = "https://" + baseUrlZ + "api/";//正式


    //======================================②分享链接路径=====测试+正式=========================================================
    //分享链接
    static String baseUrlZC = "test.m.dapengjiaoyu.com:8000/";//测试
//    private static String baseUrlZC = "m.dapengjiaoyu.com/";//正式

    public static String copyBaseUrls = "https://" + baseUrlZC;


}
