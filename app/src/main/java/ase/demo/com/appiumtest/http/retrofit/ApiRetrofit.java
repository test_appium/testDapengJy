package ase.demo.com.appiumtest.http.retrofit;


import java.util.List;
import java.util.Map;

import ase.demo.com.appiumtest.api.ApiRuestPort;
import ase.demo.com.appiumtest.bean.LodingCheckUserBean;
import ase.demo.com.appiumtest.bean.LodingUserBean;
import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface ApiRetrofit {

    //Headers加入多个heard
    @Headers({"Content-Type:application/x-www-form-urlencoded",
            "api-version:1.2"
    })
    @GET("WixinTask/account/verification")
    Call<LodingCheckUserBean> loginBeforeVersion(@QueryMap Map<String, String> map);

    @Headers({"Content-Type:application/x-www-form-urlencoded",
            "api-version:1.2"
    })
    @GET("WixinTask/account/verification")
    Call<String> loginBeforeVersionString(@QueryMap Map<String, String> map);


    //Rxjava线程控制
    @Headers({"Content-Type:application/x-www-form-urlencoded",
            "api-version:1.2"
    })
    @GET("WixinTask/account/verification")
    Observable<LodingCheckUserBean> loginBeforeVersionStringRx(@QueryMap Map<String, String> map);


    //登录
    @Headers({"Content-Type:application/x-www-form-urlencoded",
            "api-version:1.2"
    })
    @POST("WixinTask/account/login")
    @FormUrlEncoded
    Observable<LodingUserBean> loginRx(@FieldMap Map<String, String> map);


    //====================================================================================
    //==============================以下为常用示例=========================================
    //====================================================================================
    //动态heard
    @GET
    Call<String> caseHeard(@Header("api-version") String apiVersion, @QueryMap Map<String, String> map);


    //重新定义接口地址可以使用@Url
    @GET
    Call<List<String>> caseGetActivitySubjectsList(@Url String url, @QueryMap Map<String, String> map);


}
