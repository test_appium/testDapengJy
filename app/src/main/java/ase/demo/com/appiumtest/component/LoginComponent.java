package ase.demo.com.appiumtest.component;

import javax.inject.Singleton;

import ase.demo.com.appiumtest.model.LoginModel;
import ase.demo.com.appiumtest.ui.activity.LoginActivity;
import ase.demo.com.appiumtest.ui.activity.SettingsActivity;
import dagger.Component;

/**
 * module 和使用类的关联
 * Provide 如果是单例模式 对应的Compnent 也要是单例模式
 *
 * @Singleton：如果Module中指定了,Component中必须指定,否则编译报错,并且两个文件必须使用相同的
 * @dependencies依赖其他component 依赖component， component之间的Scoped 不能相同
 */
@Singleton//Provide 如果是单例模式 对应的Compnent 也要是单例模式
@Component(modules = LoginModel.class)//,dependencies =DaggerLoginComponent.class )
public interface LoginComponent {
    void inject(LoginActivity loginActivity);

    void inject(SettingsActivity loginActivity);
}
