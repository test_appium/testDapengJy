package ase.demo.com.appiumtest.api;


public class ApiRuestPort {

    public static boolean userData() {
       /* if (DaPengApplication.getUserData() != null && DaPengApplication.getUserData().getData() != null) {
            return true;
        } else {
            return false;
        }*/
        return false;
    }

    //作业接口
    public static String getJobListByInstructorPort() {
       /* if (userData()) {
            LodingUserBean userBean = DaPengApplication.getUserData();
            UserBean user = userBean.getData();
            //角色
            if (user.getUserRole() != null) {
                if (Config.ROLE_INSTRUCTOR.equals(user.getUserRole())) {
                    return "WixinTask/getJobListByInstructor";//讲师
                } else {
                    return "WixinTask/getJobListByConsultant";
                }
            }
        }*/
        return null;
    }

    //课程接口
    public static String getCurriculumPort() {
        /*if (userData()) {
            LodingUserBean userBean = DaPengApplication.getUserData();
            UserBean user = userBean.getData();
            //角色
            if (user.getUserRole() != null) {
                if (Config.ROLE_INSTRUCTOR.equals(user.getUserRole())) {
                    return "WixinTask/getCourseChannel";//讲师
                } else {
                    return "WixinTask/getCourses";
                }
            }
        }*/
        return null;
    }

    //推荐/取消推荐
    public static String getCorrectionJobByInstructor = "WixinTask/recommendJob";

    public static String getUpload_file2 = "extension/upload_file2";

    //删除评论
    public static String getDeleteCorrection = "WixinTask/deleteCorrection";

    //课程下的作业列表
    public static String getTaskList = "WixinTask/getTaskList";

    //获取当前老师下创建的所有课程
    public static String getAllCourseInstructor = "WixinTask/getAllCourseInstructor";

    //登录
    public static String getLogin = "WixinTask/account/login";
    //新增登录校验 19.2.20-v1044
    public static String getLoginCheck = "WixinTask/account/verification";
//    public static String getLogin = "api/user/user-login";

    //统计报表
    public static String getTaskStatic = "WixinTask/taskStatic";

    //作业详情
    public static String getjobsList = "WixinTask/getJobInfoByTaskIdAndUserId";

    //获取更多评论
    public static String getMoreList = "WixinTask/jobCommentList";
//    public static String getjobsList="extension/jobsList";

    //提醒
    public static String getAlert = "WixinTask/remindTeacher";

    //获取oss鉴权
    public static String getOss = "sts/authorizations";

    //上传文件到服务器
    public static String getUpload_file = "extension/upload_file";

    //获取当前课程下的所有开课期号
    public static String getAllCourseStage = "WixinTask/getAllCourseStage";

    //获取消息提醒规则
    public static String getMessageAlert = "WixinTask/getRemindSetting";

    //设置推送
    public static String getMessageSetting = "WixinTask/setUserOpenStatu";
    public static String setGradeCounselor = "WixinTask/correctionJobByInstructor";

    //批改作业接口
    public static String getCorrectionJobByInstructor() {
        /*if (userData()) {
            LodingUserBean userBean = DaPengApplication.getUserData();
            UserBean user = userBean.getData();
            //角色
            if (user.getUserRole() != null) {
                if (Config.ROLE_INSTRUCTOR.equals(user.getUserRole())) {
                    return "WixinTask/correctionJobByInstructor";//讲师
                } else {
                    return "WixinTask/correctionJobByConsultant";
                }
            }
        }*/
        return null;
    }

}
