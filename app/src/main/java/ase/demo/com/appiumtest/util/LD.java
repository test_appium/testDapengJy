package ase.demo.com.appiumtest.util;

import android.util.Log;

public class LD {
    final static String TAG = "LD";

    public static void V(String content) {
        Log.v(TAG, "" + content);
    }

    public static void D(String content) {
        Log.d(TAG, "" + content);
    }

    public static void I(String content) {
        Log.i(TAG, "" + content);
    }

    public static void W(String content) {
        Log.w(TAG, "" + content);
    }

    public static void E(String content) {
        Log.e(TAG, "" + content);
    }

}
