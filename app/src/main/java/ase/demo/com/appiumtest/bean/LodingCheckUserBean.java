package ase.demo.com.appiumtest.bean;

import java.io.Serializable;


public class LodingCheckUserBean implements Serializable{
    static final long serialVersionUID = 43L;
    String success;
    String errorCode;
    String msg;


    CheckUserBean data;


    public CheckUserBean getData() {
        return data;
    }

    public void setData(CheckUserBean data) {
        this.data = data;
    }


    public LodingCheckUserBean(String success, String errorCode, String msg) {
        this.success = success;
        this.errorCode = errorCode;
        this.msg = msg;
    }


    public LodingCheckUserBean() {
    }


    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
