package ase.demo.com.appiumtest.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import ase.demo.com.appiumtest.R;
import ase.demo.com.appiumtest.api.Api;
import ase.demo.com.appiumtest.bean.LodingCheckUserBean;
import ase.demo.com.appiumtest.http.retrofit.ApiRetrofit;
import ase.demo.com.appiumtest.util.LD;
import ase.demo.com.appiumtest.util.Tools;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitActivity extends AppCompatActivity {

    String baseUrl = Api.baseUrl;
    OkHttpClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrofit);

        //声明缓存地址和大小
        Cache cache = new Cache(this.getCacheDir(),10*1024*1024);
        //增加超时
        client = new OkHttpClient.Builder()
                //默认重试一次，若需要重试N次，则要实现拦截器
                //若需要重试N次，可以通过设置拦截器
                .retryOnConnectionFailure(true)
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                //首先要为OkHttpClient设置Cache，否则缓存不会生效
                .cache(cache)
                .build();
    }

    //同步
    public void onRetrofitHttp(View view) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                long timestamp = System.currentTimeMillis();
                Map<String, String> maps = new HashMap<String, String>();
                maps.put("account", "13500000000");
                maps.put("password", "123456a");
                maps.put("timestamp", timestamp + "");
                String signs = Tools.getSigns(maps);
                maps.put("sign", signs + "");

                Retrofit retrofit = new Retrofit.Builder()
                        //这里建议：- Base URL: 总是以/结尾；- @Url: 不要以/开头
                        .baseUrl(baseUrl)
                        //增加返回值为Gson的支持(以实体类返回)
                        //addConverterFactory必须加入否则执行无法将返回数据转换为实体类或者String,并且程序还报错
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(client)
                        .build();
                ApiRetrofit api = retrofit.create(ApiRetrofit.class);
                Call<String> response = api.loginBeforeVersionString(maps);
                try {
                    //同步
                    String content = response.execute().body();
                    LD.V("数据:" + content);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    //异步
    public void onRetrofitSysnHttp(View view) {
        long timestamp = System.currentTimeMillis();
        Map<String, String> maps = new HashMap<String, String>();
        maps.put("account", "13500000000");
        maps.put("password", "123456a");
        maps.put("timestamp", timestamp + "");
        String signs = Tools.getSigns(maps);
        maps.put("sign", signs + "");

        Retrofit retrofit = new Retrofit.Builder()
                //这里建议：- Base URL: 总是以/结尾；- @Url: 不要以/开头
                .baseUrl(baseUrl)
                //增加返回值为Gson的支持(以实体类返回)
                //addConverterFactory必须加入否则执行无法将返回数据转换为实体类或者String,并且程序还报错
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        ApiRetrofit api = retrofit.create(ApiRetrofit.class);
        Call<LodingCheckUserBean> response = api.loginBeforeVersion(maps);
        response.enqueue(new Callback<LodingCheckUserBean>() {
            @Override
            public void onResponse(Call<LodingCheckUserBean> call, Response<LodingCheckUserBean> response) {
                LD.D("返回成功---：");
            }

            @Override
            public void onFailure(Call<LodingCheckUserBean> call, Throwable t) {

            }
        });
    }
}
