package ase.demo.com.appiumtest.model;

import javax.inject.Singleton;

import ase.demo.com.appiumtest.IPresenter.IPresenterBack;
import ase.demo.com.appiumtest.IPresenter.request.LoginPersenter;
import ase.demo.com.appiumtest.bean.UserBean;
import dagger.Module;
import dagger.Provides;

/**
 * 为LoginActivity的LoginPersenter提供初始化和实例对象
 * 且构造方法提供了请求接口的后续回调方法对象
 * module类,只负责声明和传递,传递初始化时需要后续用的对象或者参数
 * 除构造方法，不参与直接调用或者间接调用
 */
@Module
public class LoginModel {

    IPresenterBack.Callback view;

    public LoginModel(IPresenterBack.Callback view) {
        this.view = view;
    }

    //使用另外一个对象为参数时,不正确的方式是直接new
    /*@Singleton
    @Provides
    public LoginPersenter provideLoginPresenter() {
        return new LoginPersenter(view,new UserBean());
    }*/

    @Singleton
    @Provides//Provide 如果是单例模式 对应的Compnent 也要是单例模式
    public LoginPersenter provideLoginPresenter(UserBean userBean) {
        //通过@Provides声明初始化new LoginPersenter(xxx)
        return new LoginPersenter(view, userBean);
    }

    @Singleton
    @Provides//Provide 如果是单例模式 对应的Compnent 也要是单例模式
    public UserBean provideGetUserBean() {
        //通过@Provides声明初始化new UserBean()
        //可以看得出来,UserBean在其他外面未初始化
        return new UserBean();
    }


}
