package ase.demo.com.appiumtest.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ase.demo.com.appiumtest.R;

public class AppServerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_server);
    }
}
