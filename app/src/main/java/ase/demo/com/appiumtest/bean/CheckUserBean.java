package ase.demo.com.appiumtest.bean;


import java.io.Serializable;
import java.util.ArrayList;


public class CheckUserBean implements Serializable{
    static final long serialVersionUID = 43L;

    String status;
    String msg;
    ArrayList<CheckUserDataBean> users;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<CheckUserDataBean> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<CheckUserDataBean> users) {
        this.users = users;
    }
}
