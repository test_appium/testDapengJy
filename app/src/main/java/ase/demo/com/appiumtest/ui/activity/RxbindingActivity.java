package ase.demo.com.appiumtest.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.jakewharton.rxbinding.widget.RxCompoundButton;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.concurrent.TimeUnit;

import ase.demo.com.appiumtest.R;
import ase.demo.com.appiumtest.util.LD;
import rx.functions.Action1;

public class RxbindingActivity extends AppCompatActivity {

    TextView showHit, button1;
    EditText input1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rxbinding);
        showHit = findViewById(R.id.showHit);
        button1 = findViewById(R.id.button1);
        input1 = findViewById(R.id.input1);
        //由rxbinding的click代替系统clicklistenter
        onButton(button1);
        onInput1();
    }

    //4655 1835 1760     530
    //4420 1760 1670     h3
    //4700 1860 1746    h8
    //4748 1933 1697    700
    //4840 1925 1718    x90

    //防频繁点击
    public void onButton(View view) {
        RxView.clicks(view)
                .throttleFirst(1, TimeUnit.SECONDS)//1秒钟之内只取一个点击事件，防抖操作
                .subscribe(new Action1<Void>() {
                    @Override
                    public void call(Void aVoid) {
                        showHit.setText("这里是防止频繁点击后显示的内容");
                    }
                });


    }

    private void onInput1() {
        RxTextView.textChanges(input1)
                .subscribe(new Action1<CharSequence>() {
                    @Override
                    public void call(CharSequence charSequence) {
                     LD.I("--->输入了："+charSequence);
                    }
                });
    }

}
