package ase.demo.com.appiumtest.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.daimajia.numberprogressbar.NumberProgressBar;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import ase.demo.com.appiumtest.HookHelper;
import ase.demo.com.appiumtest.R;
import ase.demo.com.appiumtest.down.DbHelper;
import ase.demo.com.appiumtest.down.DownLoaderManger;
import ase.demo.com.appiumtest.down.FileInfo;
import ase.demo.com.appiumtest.down.OnProgressListener;
import cn.bingoogolapple.progressbar.BGAProgressBar;

public class Main2Activity extends AppCompatActivity implements OnProgressListener ,View.OnClickListener{


    TextView start, pause;
    private DownLoaderManger downLoader = null;
    private FileInfo info;
    NumberProgressBar numberBar;
    BGAProgressBar numberBar2;
    Button pauses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        start = (TextView) findViewById(R.id.start);
        pause = (TextView) findViewById(R.id.pause);
        numberBar = (NumberProgressBar) findViewById(R.id.numberBar);
        numberBar2 = (BGAProgressBar) findViewById(R.id.numberBar2);
        pauses = (Button) findViewById(R.id.pauses);



        final DbHelper helper = new DbHelper(this);
        downLoader = DownLoaderManger.getInstance(helper, this);
        String apkName = getCurrentYMD();
        info = new FileInfo(apkName + ".apk", "http://www.pgyer.com/app/installUpdate/6c5a5eaf3eae86c99a79e7d68d97ed7e?sig=FoG9FtIewMvlp7TWzHYtGoUiGjp5mpQt7yKGMZ7MM9iKVsT8EUGX7xi1irUTOZkK");
        downLoader.addTask(info);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (downLoader.getCurrentState(info.getUrl())) {
                    downLoader.stop(info.getUrl());
                    start.setText("开始下载");
                } else {
                    downLoader.start(info.getUrl());
                    start.setText("暂停下载");
                }
            }
        });

        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downLoader.restart(info.getUrl());
                pause.setText("暂停下载");
            }
        });


        //========hook=1===============================
        pauses.setOnClickListener(this);
        try {
            HookHelper.hookOnClickListener(pauses);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getCurrentYMD() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
        Date dt = new Date(System.currentTimeMillis());
        String date = sdf.format(dt);
        if (date.length() > 1) {
            date = date.substring(1, date.length());
        }
        return TextUtils.isEmpty(date) ? "nd" : date;
    }

    @Override
    public void updateProgress(int max, int progress) {
        /*numberBar.setMax(max);
        numberBar.incrementProgressBy(progress);

        numberBar2.setMax(max);
        numberBar2.incrementProgressBy(progress);*/
        Log.d("LD", "下载进度:" + progress + "/" + max);
    }

    @Override
    public void onSuccess(File file) {
        Log.e("LD", "下载完成:" + file.toString());
        //numberBar.setProgress(0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pauses:
                Log.d("LD","------------按钮");
                break;
        }

    }
}
