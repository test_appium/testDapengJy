package ase.demo.com.appiumtest.model;

import javax.inject.Singleton;

import ase.demo.com.appiumtest.bean.DaggerUser;
import ase.demo.com.appiumtest.bean.UserBean;
import ase.demo.com.appiumtest.bean.UserDataBean;
import dagger.Module;
import dagger.Provides;

@Module
public class DaggerUserModel {

    public DaggerUserModel() {
    }

    @Singleton
    @Provides
    public DaggerUser provideDuser() {
        return new DaggerUser();
    }

    @Provides
    public UserBean providerUserBean() {
        return new UserBean();
    }

    @Provides
    public UserDataBean providerUserDataBean() {
        return new UserDataBean();
    }

}
