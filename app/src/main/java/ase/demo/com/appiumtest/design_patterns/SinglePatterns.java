package ase.demo.com.appiumtest.design_patterns;

public class SinglePatterns {
    private SinglePatterns() {
    }

    //1、饿汉形式
    private static SinglePatterns sPatterns = new SinglePatterns();
    public static SinglePatterns getSigleton() {
        return sPatterns;
    }

    //2、懒汉形式
    private static SinglePatterns sPatterns2 ;
    public SinglePatterns  getSigleton2(){
        if (sPatterns2==null) {
            sPatterns2 =new SinglePatterns();
        }
        return sPatterns2;
    }

    //3、推荐形式（双层检测）
    private static volatile SinglePatterns sPatterns3 ;
    public SinglePatterns getPatterns3(){
        if(sPatterns3==null){
            synchronized (SinglePatterns.class){
                if(sPatterns3==null){
                    sPatterns3 =new SinglePatterns();
                }
            }
        }
        return sPatterns3;
    }




}
