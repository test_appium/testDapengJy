package ase.demo.com.appiumtest.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import ase.demo.com.appiumtest.R;
import ase.demo.com.appiumtest.api.Api;
import ase.demo.com.appiumtest.bean.LodingCheckUserBean;
import ase.demo.com.appiumtest.bean.LodingUserBean;
import ase.demo.com.appiumtest.http.retrofit.ApiRetrofit;
import ase.demo.com.appiumtest.util.LD;
import ase.demo.com.appiumtest.util.Tools;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RxJavaActivity extends AppCompatActivity {
    String TAG = "LD";
    String baseUrl = Api.baseUrl;
    //使用Defer订阅时创建观察者
    int tag = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ra_java);
    }


    //基础用法
    public void caseBase1(View view) {
        // 步骤1：创建被观察者 Observable & 生产事件
        // 即 顾客入饭店 - 坐下餐桌 - 点菜

        //  1. 创建被观察者 Observable 对象
        Observable<Integer> observable = Observable.create(new ObservableOnSubscribe<Integer>() {
            // 2. 在复写的subscribe（）里定义需要发送的事件
            @Override
            public void subscribe(ObservableEmitter<Integer> emitter) throws Exception {
                // 通过 ObservableEmitter类对象产生事件并通知观察者
                // ObservableEmitter类介绍
                // a. 定义：事件发射器
                // b. 作用：定义需要发送的事件 & 向观察者发送事件
                emitter.onNext(1);
                emitter.onNext(2);
                emitter.onNext(3);
                emitter.onComplete();
            }
        });

        // 步骤2：创建观察者 Observer 并 定义响应事件行为
        // 即 开厨房 - 确定对应菜式
        Observer<Integer> observer = new Observer<Integer>() {
            // 通过复写对应方法来 响应 被观察者
            @Override
            public void onSubscribe(Disposable d) {
                Log.d(TAG, "开始采用subscribe连接");
            }
            // 默认最先调用复写的 onSubscribe（）

            @Override
            public void onNext(Integer value) {
                Log.d(TAG, "对Next事件" + value + "作出响应");
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "对Error事件作出响应");
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "对Complete事件作出响应");
            }
        };


        // 步骤3：通过订阅（subscribe）连接观察者和被观察者
        // 即 顾客找到服务员 - 点菜 - 服务员下单到厨房 - 厨房烹调
        observable.subscribe(observer);


        //基于事件流的链式调用方式
        Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> e) throws Exception {

                e.onNext("泛型是什么类型,Next中就传入什么类型1");
                e.onNext("泛型是什么类型,Next中就传入什么类型2");
                e.onNext("泛型是什么类型,Next中就传入什么类型3");
                e.onNext("泛型是什么类型,Next中就传入什么类型4");
                e.onComplete();
            }
        }).subscribe(new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {

                LD.W("开始订阅-------");
            }


            @Override
            public void onNext(String s) {
                LD.I("相应Next事件-------" + s);

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
                LD.V("相应Complete事件-------");

            }
        });
    }

    //快速创建--just 最多10个参数
    public void caseQuickJust(View view) {
        Observable.just("" + 1, "" + 2, "" + 3, "字符串").subscribe(new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(String serializable) {
                LD.D("Just--->>:" + serializable);

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void caseDefer(View v) {
        //Observable.create()
        //此时观察者没有被创建
        Observable oba = Observable.defer(new Callable<ObservableSource<?>>() {
            @Override
            public ObservableSource<?> call() throws Exception {
                return Observable.just(tag);
            }
        });
        tag = 88;
        //此时观察者才被创建
        oba.subscribe(new Observer() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Object o) {
                LD.D("Defer---------->>" + ((Integer) o));
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });


    }

    //延迟自动发送next
    public void caseTimer(View v) {
        Observable.timer(3, TimeUnit.SECONDS).subscribe(new Observer<Long>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Long aLong) {
                Log.d(TAG, "接收到了事件" + aLong);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });

    }
    //关于事件的有
    //2、interval每隔指定时间 就发送 事件   interval默认在computation调度器上执行
    //Observable.interval(3,1,TimeUnit.SECONDS) 参1 = 第1次延迟时间 参2 = 间隔时间数字 参3 = 时间单位
    //发送的事件序列 = 从0开始、无限递增1的的整数序列
    //3、intervalRange每隔指定时间 就发送 事件，可指定发送的数据的数量
    //Observable.intervalRange(3,10,2, 1, TimeUnit.SECONDS) 参1 = 事件序列起始点 参数2 = 事件数量 参数3 = 第1次事件延迟发送时间 参数4 = 间隔时间数字 参数5 = 时间单位
    //发送的事件序列 = 从0开始、无限递增1的的整数序列 作用类似于interval（），但可指定发送的数据的数量


    //线程控制
    //通过指定Observable/Observer的线程,来明确被观察者和观察者处理事件所在线程类型
    public void caseThreadControl(View view) {

    }

    //简单的Retrofit+RxJava请求
    public void caseSubOb(View view) {
        long timestamp = System.currentTimeMillis();
        Map<String, String> maps = new HashMap<String, String>();
        maps.put("account", "13500000000");
        maps.put("password", "123456a");
        maps.put("timestamp", timestamp + "");
        String signs = Tools.getSigns(maps);
        maps.put("sign", signs + "");

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()) // 支持RxJava
                .baseUrl(baseUrl)
                .build();
        ApiRetrofit apiRetrofit = retrofit.create(ApiRetrofit.class);
        Observable<LodingCheckUserBean> observable = apiRetrofit.loginBeforeVersionStringRx(maps);
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<LodingCheckUserBean>() {

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(LodingCheckUserBean lodingCheckUserBean) {
                        LD.W("数据到了：" + lodingCheckUserBean.toString());
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }

    //嵌套请求 即多个请求 请求1成功后去执行请求2...
    public void caseNestHttp(View view) {
        long timestamp = System.currentTimeMillis();
        Map<String, String> maps = new HashMap<String, String>();
        maps.put("account", "13500000000");
        maps.put("password", "123456a2");
        maps.put("timestamp", timestamp + "");
        String signs = Tools.getSigns(maps);
        maps.put("sign", signs + "");
        //----------------------------
        final Map<String, String> loginMap = new HashMap<String, String>();
        loginMap.put("userId", "jsph651gkq");
        loginMap.put("password", "123456a");
        loginMap.put("timestamp", System.currentTimeMillis() + "");
        String loginMapSigns = Tools.getSigns(loginMap);
        loginMap.put("sign", loginMapSigns);


        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()) // 支持RxJava
                .baseUrl(baseUrl)
                .build();

        final ApiRetrofit apiRetrofit = retrofit.create(ApiRetrofit.class);
        //嵌套一
        final Observable<LodingUserBean> login = apiRetrofit.loginRx(loginMap);

        apiRetrofit.loginBeforeVersionStringRx(maps)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<LodingCheckUserBean>() {
                    @Override
                    public void accept(LodingCheckUserBean lodingCheckUserBean) throws Exception {
                        Log.d(TAG, "校验-------------第1次网络请求成功");
                    }
                })
                .observeOn(Schedulers.io())// （新被观察者，同时也是新观察者）切换到IO线程去发起登录请求
                // 特别注意：因为flatMap是对初始被观察者作变换，所以对于旧被观察者，它是新观察者，所以通过observeOn切换线程
                // 但对于初始观察者，它则是新的被观察者
                .flatMap(new Function<LodingCheckUserBean, ObservableSource<LodingUserBean>>() { // 作变换，即作嵌套网络请求
                    @Override
                    public ObservableSource<LodingUserBean> apply(LodingCheckUserBean result) throws Exception {
                        // 将网络请求1转换成网络请求2，即发送网络请求2
                        //嵌套二
                        /*LD.W("打印userID:"+result.getData().getUsers().get(0).getUserId());
                        Observable<LodingUserBean> login = apiRetrofit.loginRx(loginMap);*/
                        return login;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<LodingUserBean>() {
                    @Override
                    public void accept(LodingUserBean lodingUserBean) throws Exception {
                        Log.d(TAG, "第2次网络请求成功");
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        System.out.println("登录失败");
                    }
                })
        ;
    }


    //剔除集合中的'第四'
    public void caseFilter(View view) {

        ArrayList<String> list = new ArrayList<>();
        list.add("测试1");
        list.add("测试2");
        list.add("测试3");
        list.add("第四");
        list.add("测试5");
        list.add("测试6");

        ArrayList<String> lists = new ArrayList<>();
        Observable.fromIterable(list)
                .filter(new Predicate<String>() {
                    @Override
                    public boolean test(String s) throws Exception {
                        return !"第四".equals(s);
                    }
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(Schedulers.newThread())
                .map(new Function<String, ArrayList<String>>() {
                    @Override
                    public ArrayList<String> apply(String s) throws Exception {
                        LD.I("map:" + s);
                        lists.add(s);
                        return lists;
                    }
                }).subscribe(new Consumer<ArrayList<String>>() {
            @Override
            public void accept(ArrayList<String> strings) throws Exception {
                for (String string : strings) {
                    LD.D("accept:" + string);
                }
            }
        });

        /*Observable.create(new ObservableOnSubscribe<ArrayList<String>>() {
            @Override
            public void subscribe(ObservableEmitter<ArrayList<String>> emitter) throws Exception {
                emitter.onNext(list);
            }
        }).filter(new Predicate<ArrayList<String>>() {
            @Override
            public boolean test(ArrayList<String> strings) throws Exception {
                return false;
            }
        }).map(new Function<ArrayList<String>, ArrayList<String>>() {
            @Override
            public ArrayList<String> apply(ArrayList<String> strings) throws Exception {
                return null;
            }
        }).subscribe(new Consumer<ArrayList<String>>() {
            @Override
            public void accept(ArrayList<String> strings) throws Exception {

            }
        });*/


    }


}
