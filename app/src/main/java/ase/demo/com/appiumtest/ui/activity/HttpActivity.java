package ase.demo.com.appiumtest.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.HashMap;
import java.util.Map;

import ase.demo.com.appiumtest.R;
import ase.demo.com.appiumtest.api.ApiRuestPort;
import ase.demo.com.appiumtest.bean.LodingCheckUserBean;
import ase.demo.com.appiumtest.util.LD;
import ase.demo.com.appiumtest.util.Tools;
import callbacks.ResultCallbackAdapt;
import http.ApiException;
import http.HttpRequestFactory;
import kaiqi.cn.httpx.resp.RegisterResp;
import request.CommonRequest;
import resp.HttpCommObjResp;

public class HttpActivity extends AppCompatActivity {


    public class RequestData extends CommonRequest {
        String account;
        String password;
        String timestamp;
        String sign;

        public RequestData() {
        }

        public RequestData(String account, String password, String timestamp, String sign) {
            this.account = account;
            this.password = password;
            this.timestamp = timestamp;
            this.sign = sign;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        @Override
        public String postfix() {
            return ApiRuestPort.getLoginCheck;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_http);
        long timestamp = System.currentTimeMillis();
        Map<String, String> maps = new HashMap<String, String>();
        maps.put("account", "13500000000");
        maps.put("password", "123456a");
        maps.put("timestamp", timestamp + "");
        String signs = Tools.getSigns(maps);
        maps.put("sign", signs + "");

        RequestData re = new RequestData("13500000000", "123456a", timestamp + "", signs);

        /*HttpRequestFactory.doPost(timestamp, new ResultCallbackAdapt<HttpCommObjResp<RegisterResp>>() {
            @Override
            public void doOnResponse(HttpCommObjResp<RegisterResp> response) {
                System.out.println(response + "");
                LD.E("\n方式一返回:" + response);
            }

            @Override
            public void doOnError(ApiException ex) {
                System.out.println(ex + "");
                LD.E("\n方式一返回:" + ex);
            }
        }, iLoading);*/

        // 方式二
        HttpRequestFactory.doGet(re, new ResultCallbackAdapt<LodingCheckUserBean>() {
            @Override
            public void doOnResponse(LodingCheckUserBean response) {
                System.out.println(response + "");
                LD.E("\n方式二返回:" + response + "\nUserID" + response.getData().getUsers().get(0).getUserId());
            }

            @Override
            public void doOnError(ApiException ex) {
                System.out.println(ex + "");
                LD.E("\n方式二返回:" + ex);
            }
        });
    }
}
