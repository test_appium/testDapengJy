package ase.demo.com.appiumtest.bean;



import java.io.Serializable;


public class LodingUserBean implements Serializable{
    static final long serialVersionUID = 42L;

    String success;
    String errorCode;
    String msg;

    String status;
    String errorMsg;

    UserBean data;


    public LodingUserBean(String success, String errorCode, String msg,
                          String status, String errorMsg, UserBean data) {
        this.success = success;
        this.errorCode = errorCode;
        this.msg = msg;
        this.status = status;
        this.errorMsg = errorMsg;
        this.data = data;
    }

    public LodingUserBean() {
    }


    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public UserBean getData() {
        return data;
    }

    public void setData(UserBean data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
