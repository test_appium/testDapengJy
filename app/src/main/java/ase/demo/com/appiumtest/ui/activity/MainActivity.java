package ase.demo.com.appiumtest.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import ase.demo.com.appiumtest.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
    }


    public void onOkhttp(View view) {
        startActivity(OkHttpActivity.class);
    }

    public void onRetrofit(View view) {
        startActivity(RetrofitActivity.class);
    }

    public void onDownload(View view) {
        startActivity(Main2Activity.class);
    }

    public void onRxJava(View view) {
        startActivity(RxJavaActivity.class);
    }

    public void onDagger(View view) {
        startActivity(DaggerMainActivity.class);
    }

    public void onHttp(View view) {
        startActivity(HttpActivity.class);
    }

    public void onRxbinding(View view) {
        startActivity(RxbindingActivity.class);
    }

    public void onDoKV(View view) {
        startActivity(DoKVMainActivity.class);
    }
    public void onAppServer(View view) {
        startActivity(AppServerActivity.class);
    }


    //======================================================
    public void startActivity(Class mClass) {
        startActivity(new Intent(MainActivity.this, mClass));
    }
}
