package ase.demo.com.appiumtest.ui.activity;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import ase.demo.com.appiumtest.R;
import ase.demo.com.appiumtest.api.Api;
import ase.demo.com.appiumtest.api.ApiRuestPort;
import ase.demo.com.appiumtest.interfaces.CommonLogs;
import ase.demo.com.appiumtest.interfaces.XInterceptor;
import ase.demo.com.appiumtest.util.LD;
import ase.demo.com.appiumtest.util.Tools;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OkHttpActivity extends AppCompatActivity {


    private static String baseUrl = Api.baseUrl;
    Response response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ok_http);
    }

    String url;

    //同步基本用法
    public void onOkhttpBase(View view) {
        long timestamp = System.currentTimeMillis();
        Map<String, String> maps = new HashMap<String, String>();
        maps.put("account", "讲师35");
        maps.put("password", "123456");
        maps.put("timestamp", timestamp + "");
        String signs = Tools.getSigns(maps);

        url = baseUrl + ApiRuestPort.getLoginCheck + "?account=讲师35&password=123456&timestamp=" + timestamp + "&sign=" + signs;
        LD.D("网络:" + url);
        new Thread(new Runnable() {
            @Override
            public void run() {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
//                        .url("http://www.baidu.com")
                        .url(url)
                        .build();
                try {
                    response = client.newCall(request).execute();
                    if (response != null && response.isSuccessful()) {
                        //http协议中自带的
                        int code = response.code();
                        String mess = response.message();
                        LD.W("网络状态:" + code + "/" + mess);

                        //服务器
                        String content = response.body().toString();
                        LD.V("服务器#" + content);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    LD.E(e.getMessage());
                }
            }
        }).start();
    }


    //异步GET（参数拼接到url后）
    public void onOkhttpSyncBase(View view) {


        long timestamp = System.currentTimeMillis();
        Map<String, String> maps = new HashMap<String, String>();
        maps.put("account", "13500000000");
        maps.put("password", "123456a");
        maps.put("timestamp", timestamp + "");
        String signs = Tools.getSigns(maps);

        //拼接参数
        url = baseUrl + ApiRuestPort.getLoginCheck + "?timestamp=" + timestamp + "&password=123456a&account=13500000000";
       // LD.D("网络:" + url);

        //get参数不可通过method+body
        RequestBody body = new FormBody.Builder()
                .add("account", "讲师35")
                .add("password", "123456")
                .add("timestamp", "" + timestamp)
                .add("sign", "" + signs)
                .build();

        OkHttpClient client = new OkHttpClient().newBuilder()
                .addInterceptor(new XInterceptor.CommonLog()).build();


        Request request = new Request.Builder()
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("api-version", "1.2")
                .addHeader("sign", "" + signs)
                .url(url)
                /*.url(baseUrl + ApiRuestPort.getLoginCheck)
                .method("GET", body)*/
                .build();
        try {
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    LD.E("服务器异常#" + e.getMessage());
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    LD.V("onResponse~~~~~~~~~~~~~");
                    if (response != null && response.isSuccessful()) {
                        //http协议中自带的
                        int code = response.code();
                        String mess = response.message();
                        LD.W("网络状态:" + code + "/" + mess);

                        //服务器
                        String content = response.body().string();
                        LD.V("服务器#" + content);
                    }
                }

            });

        } catch (Exception e) {
            e.printStackTrace();
            LD.E(e.getMessage());
        }

    }

    //异步POST
    public void onOkhttpSyncPostBase(View view) {
        long timestamp = System.currentTimeMillis();
        Map<String, String> maps = new HashMap<String, String>();
        maps.put("userId", "jsph651gkq");
        maps.put("password", "123456a");
        maps.put("timestamp", timestamp + "");
        String signs = Tools.getSigns(maps);

        //拼接参数
        url = baseUrl + ApiRuestPort.getLogin;
        LD.D("网络:" + url);

        //get参数不可通过method+body
        RequestBody body = new FormBody.Builder()
                .add("timestamp", "" + timestamp)
                .add("password", "123456a")
                .add("userId", "jsph651gkq")
                .add("sign", signs)
                .build();

        OkHttpClient client = new OkHttpClient().newBuilder().addInterceptor(new InterceptorOkHttp()).build();

        Request request = new Request.Builder()
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("api-version", "1.2")
                .addHeader("sign", "" + signs)
                .url(url)
                .post(body)
                .build();
        try {
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    LD.E("服务器异常#" + e.getMessage());
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    LD.V("post onResponse~~~~~~~~~~~~~");
                    if (response != null && response.isSuccessful()) {
                        //http协议中自带的
                        int code = response.code();
                        String mess = response.message();
                        LD.W("post 网络状态:" + code + "/" + mess);
                        //服务器
                        String content = response.body().string();
                        LD.V("post 服务器#" + content);
                    }
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
            LD.E(e.getMessage());
        }

    }


    //异步GET下载文件
    public void onOkhttpSyncGETFileBase(View view) {
        //拼接参数
        url = "https://www.baidu.com/img/bd_logo1.png";
        LD.D("网络:" + url);
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(15 * 1000, TimeUnit.MILLISECONDS)
                .readTimeout(15 * 1000, TimeUnit.MILLISECONDS)
                .writeTimeout(15 * 1000, TimeUnit.MILLISECONDS)
                .addInterceptor(new InterceptorOkHttp())
//                .addNetworkInterceptor()
                .build();

        Request request = new Request.Builder()
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .url(url)
                .build();

        try {
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    LD.E("服务器异常#" + e.getMessage());
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    LD.V("onResponse~~~~~~~~~~~~~");
                    if (response != null && response.isSuccessful()) {
                        //http协议中自带的
                        int code = response.code();
                        String mess = response.message();
                        LD.W("网络状态:" + code + "/" + mess);
                        //服务器
                        InputStream inputStream = response.body().byteStream();
                        if (inputStream != null) {
                            int leng;
                            File file = new File(Environment.getExternalStorageDirectory(), "testB.png");
                            FileOutputStream fos = new FileOutputStream(file);
                            byte[] buff = new byte[1024];
                            while ((leng = inputStream.read(buff)) != -1) {
                                fos.write(buff, 0, leng);
                            }
                            fos.flush();
                            fos.close();
                            inputStream.close();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            LD.E(e.getMessage());
        }

    }

}
