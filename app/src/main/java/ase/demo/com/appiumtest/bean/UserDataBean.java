package ase.demo.com.appiumtest.bean;


import java.io.Serializable;

import ase.demo.com.appiumtest.util.LD;

public class UserDataBean implements Serializable {
    static final long serialVersionUID = 42L;

    private String userId;
    private String schoolId;
    private String email;
    private String nickname;
    private String mobile;
    private String gender;
    private String trueName;
    private String address;
    private String familyAddress;
    private String occupation;
    private String position;
    private String isTrialCourse;
    private String school;
    private String qqAccount;
    private String wxOpenId;
    private String qqOpenId;
    private String weixinAccount;
    private String className;
    private String avatar;
    private String signature;
    private int payBalance;
    private String courseAdvisor;
    private String affiliatedCollege;
    private String payAnswer;
    private String status;
    private String isInstructor;
    private String isAdministrator;
    private long dateAdded;
    private long lastModified;
    private long lastLoginTime;
    private String isVip;
    private String vipExpirationDate;
    private String registerWay;
    private String protocolStatus;
    private String introduction;
    private String categoryId;
    private int flower;
    private int integral;
    private String learningNumberOfDays;
    private String learningTime;
    private String registerSource;
    private String safetyGrade;
    private String spare;
    private String clueCategory;
    private String spare3;
    private String spare4;







    public UserDataBean(String userId, String schoolId, String email,
            String nickname, String mobile, String gender, String trueName,
            String address, String familyAddress, String occupation,
            String position, String isTrialCourse, String school, String qqAccount,
            String wxOpenId, String qqOpenId, String weixinAccount,
            String className, String avatar, String signature, int payBalance,
            String courseAdvisor, String affiliatedCollege, String payAnswer,
            String status, String isInstructor, String isAdministrator,
            long dateAdded, long lastModified, long lastLoginTime, String isVip,
            String vipExpirationDate, String registerWay, String protocolStatus,
            String introduction, String categoryId, int flower, int integral,
            String learningNumberOfDays, String learningTime, String registerSource,
            String safetyGrade, String spare, String clueCategory, String spare3,
            String spare4) {
        this.userId = userId;
        this.schoolId = schoolId;
        this.email = email;
        this.nickname = nickname;
        this.mobile = mobile;
        this.gender = gender;
        this.trueName = trueName;
        this.address = address;
        this.familyAddress = familyAddress;
        this.occupation = occupation;
        this.position = position;
        this.isTrialCourse = isTrialCourse;
        this.school = school;
        this.qqAccount = qqAccount;
        this.wxOpenId = wxOpenId;
        this.qqOpenId = qqOpenId;
        this.weixinAccount = weixinAccount;
        this.className = className;
        this.avatar = avatar;
        this.signature = signature;
        this.payBalance = payBalance;
        this.courseAdvisor = courseAdvisor;
        this.affiliatedCollege = affiliatedCollege;
        this.payAnswer = payAnswer;
        this.status = status;
        this.isInstructor = isInstructor;
        this.isAdministrator = isAdministrator;
        this.dateAdded = dateAdded;
        this.lastModified = lastModified;
        this.lastLoginTime = lastLoginTime;
        this.isVip = isVip;
        this.vipExpirationDate = vipExpirationDate;
        this.registerWay = registerWay;
        this.protocolStatus = protocolStatus;
        this.introduction = introduction;
        this.categoryId = categoryId;
        this.flower = flower;
        this.integral = integral;
        this.learningNumberOfDays = learningNumberOfDays;
        this.learningTime = learningTime;
        this.registerSource = registerSource;
        this.safetyGrade = safetyGrade;
        this.spare = spare;
        this.clueCategory = clueCategory;
        this.spare3 = spare3;
        this.spare4 = spare4;
    }

    public UserDataBean() {
        LD.D("new UserDataBean()");
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFamilyAddress() {
        return familyAddress;
    }

    public void setFamilyAddress(String familyAddress) {
        this.familyAddress = familyAddress;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getIsTrialCourse() {
        return isTrialCourse;
    }

    public void setIsTrialCourse(String isTrialCourse) {
        this.isTrialCourse = isTrialCourse;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getQqAccount() {
        return qqAccount;
    }

    public void setQqAccount(String qqAccount) {
        this.qqAccount = qqAccount;
    }

    public String getWxOpenId() {
        return wxOpenId;
    }

    public void setWxOpenId(String wxOpenId) {
        this.wxOpenId = wxOpenId;
    }

    public String getQqOpenId() {
        return qqOpenId;
    }

    public void setQqOpenId(String qqOpenId) {
        this.qqOpenId = qqOpenId;
    }

    public String getWeixinAccount() {
        return weixinAccount;
    }

    public void setWeixinAccount(String weixinAccount) {
        this.weixinAccount = weixinAccount;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public int getPayBalance() {
        return payBalance;
    }

    public void setPayBalance(int payBalance) {
        this.payBalance = payBalance;
    }

    public String getCourseAdvisor() {
        return courseAdvisor;
    }

    public void setCourseAdvisor(String courseAdvisor) {
        this.courseAdvisor = courseAdvisor;
    }

    public String getAffiliatedCollege() {
        return affiliatedCollege;
    }

    public void setAffiliatedCollege(String affiliatedCollege) {
        this.affiliatedCollege = affiliatedCollege;
    }

    public String getPayAnswer() {
        return payAnswer;
    }

    public void setPayAnswer(String payAnswer) {
        this.payAnswer = payAnswer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsInstructor() {
        return isInstructor;
    }

    public void setIsInstructor(String isInstructor) {
        this.isInstructor = isInstructor;
    }

    public String getIsAdministrator() {
        return isAdministrator;
    }

    public void setIsAdministrator(String isAdministrator) {
        this.isAdministrator = isAdministrator;
    }

    public long getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(long dateAdded) {
        this.dateAdded = dateAdded;
    }

    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    public long getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(long lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getIsVip() {
        return isVip;
    }

    public void setIsVip(String isVip) {
        this.isVip = isVip;
    }

    public String getVipExpirationDate() {
        return vipExpirationDate;
    }

    public void setVipExpirationDate(String vipExpirationDate) {
        this.vipExpirationDate = vipExpirationDate;
    }

    public String getRegisterWay() {
        return registerWay;
    }

    public void setRegisterWay(String registerWay) {
        this.registerWay = registerWay;
    }

    public String getProtocolStatus() {
        return protocolStatus;
    }

    public void setProtocolStatus(String protocolStatus) {
        this.protocolStatus = protocolStatus;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public int getFlower() {
        return flower;
    }

    public void setFlower(int flower) {
        this.flower = flower;
    }

    public int getIntegral() {
        return integral;
    }

    public void setIntegral(int integral) {
        this.integral = integral;
    }

    public String getLearningNumberOfDays() {
        return learningNumberOfDays;
    }

    public void setLearningNumberOfDays(String learningNumberOfDays) {
        this.learningNumberOfDays = learningNumberOfDays;
    }

    public String getLearningTime() {
        return learningTime;
    }

    public void setLearningTime(String learningTime) {
        this.learningTime = learningTime;
    }

    public String getRegisterSource() {
        return registerSource;
    }

    public void setRegisterSource(String registerSource) {
        this.registerSource = registerSource;
    }

    public String getSafetyGrade() {
        return safetyGrade;
    }

    public void setSafetyGrade(String safetyGrade) {
        this.safetyGrade = safetyGrade;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare;
    }

    public String getClueCategory() {
        return clueCategory;
    }

    public void setClueCategory(String clueCategory) {
        this.clueCategory = clueCategory;
    }

    public String getSpare3() {
        return spare3;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public String getSpare4() {
        return spare4;
    }

    public void setSpare4(String spare4) {
        this.spare4 = spare4;
    }

}
