package ase.demo.com.appiumtest.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

public class Tools {
    static String APK_LOGDING = "8934031001776A04444F72154425DDBC";

    public static String getSigns(Map<String, String> map) {
        StringBuffer stringBuffer = new StringBuffer();
        if (map == null || map.isEmpty()) {
            return null;
        }
        Map<String, String> sortMap = new TreeMap<String, String>(new MapKeyComparator());
        sortMap.putAll(map);
        for (Map.Entry<String, String> entry : sortMap.entrySet()) {
            stringBuffer.append(entry.getKey() + entry.getValue());
        }
        String signs = getMd5Value(APK_LOGDING + stringBuffer.toString() + APK_LOGDING).toUpperCase();
        return signs ;
    }

    public static class MapKeyComparator implements Comparator<String> {
        public int compare(String str1, String str2) {
            return str1.compareTo(str2);
        }
    }

    public static String getMd5Value(String sSecret) {
        try {
            MessageDigest bmd5 = MessageDigest.getInstance("MD5");
            bmd5.update(sSecret.getBytes());
            int i;
            StringBuffer buf = new StringBuffer();
            byte[] b = bmd5.digest();
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            return buf.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
