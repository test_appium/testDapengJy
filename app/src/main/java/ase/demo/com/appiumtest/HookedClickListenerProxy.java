package ase.demo.com.appiumtest;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class HookedClickListenerProxy implements View.OnClickListener {

    private View.OnClickListener origin;

    public HookedClickListenerProxy(View.OnClickListener origin) {
        this.origin = origin;
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(v.getContext(), "Hook Click Listener", Toast.LENGTH_SHORT).show();
        Log.d("LD","------------按钮onClick");
        if (origin != null) {
            origin.onClick(v);
        }
    }

}
