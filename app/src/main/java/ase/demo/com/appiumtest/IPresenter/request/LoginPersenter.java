package ase.demo.com.appiumtest.IPresenter.request;

import ase.demo.com.appiumtest.IPresenter.IPresenterBack;
import ase.demo.com.appiumtest.bean.UserBean;
import ase.demo.com.appiumtest.util.LD;


/**
 * 接口类:
 * 为LoginActivity请求接口
 */
public class LoginPersenter implements IPresenterBack.Presenter {

    IPresenterBack.Callback view;
    UserBean userBean;//不同的Activity 对象还是不一样的

    public LoginPersenter(IPresenterBack.Callback view, UserBean userBean) {
        this.view = view;
        this.userBean = userBean;
        LD.I("打印:" + this.userBean.toString());
    }

    //检测账号是否是服务器中认证账号
    public void checkLogin(String name, String pwd) {

        userBean.setUserRole("角色");
        //请求接口
        LD.E("测试:" + name + pwd + "===" + userBean.getUserRole());

        //请求成功后的回调
        view.finish(null);


    }
}
