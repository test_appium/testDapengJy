package ase.demo.com.appiumtest.bean;


import java.io.Serializable;

import ase.demo.com.appiumtest.util.LD;
import leavesc.hello.dokv.annotation.DoKV;

@DoKV
public class UserBean  implements Serializable {
    static final long serialVersionUID = 42L;

    String openStatu;
    String userRole;
    String trialCourse;

    UserDataBean  user;

    public UserBean(){
        LD.E("new UserBean()");
    }

    public UserBean(String openStatu, String userRole, String trialCourse,
            UserDataBean user) {
        this.openStatu = openStatu;
        this.userRole = userRole;
        this.trialCourse = trialCourse;
        this.user = user;
    }



    public String getOpenStatu() {
        return openStatu;
    }

    public void setOpenStatu(String openStatu) {
        this.openStatu = openStatu;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getTrialCourse() {
        return trialCourse;
    }

    public void setTrialCourse(String trialCourse) {
        this.trialCourse = trialCourse;
    }

    public UserDataBean getUser() {
        return this.user;
    }

    public void setUser(UserDataBean user) {
        this.user = user;
    }




}
