package ase.demo.com.appiumtest.bean;

import java.io.Serializable;


public class CheckUserDataBean implements Serializable {
    static final long serialVersionUID = 43L;

    private String userId;
    private String type;
    private String account;
    private String mobile;
    private String loginName;
    private String nickName;

    public CheckUserDataBean() {
    }

    public CheckUserDataBean(String userId, String type, String account, String mobile, String loginName, String nickName) {
        this.userId = userId;
        this.type = type;
        this.account = account;
        this.mobile = mobile;
        this.loginName = loginName;
        this.nickName = nickName;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
