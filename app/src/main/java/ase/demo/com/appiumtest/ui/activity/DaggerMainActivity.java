package ase.demo.com.appiumtest.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;
import javax.inject.Provider;

import ase.demo.com.appiumtest.R;
import ase.demo.com.appiumtest.bean.DaggerUser;
import ase.demo.com.appiumtest.bean.UserBean;
import ase.demo.com.appiumtest.bean.UserDataBean;
import ase.demo.com.appiumtest.interfaces.DaggerDaggerMainComponent;
import ase.demo.com.appiumtest.model.DaggerUserModel;
import dagger.Lazy;
import dagger.Provides;

public class DaggerMainActivity extends AppCompatActivity {

    @Inject
    DaggerUser dUser;
    TextView showMess;

    //懒加载，初始完成后,并不会实例化一个对象,而且当使用mUserBeanLazy.get时才会
    //并且每次调用的对象都会是同一个(未使用Singleton修饰，当修饰了则每次都会调用的是同一个对象)
    @Inject
    Lazy<UserBean> mUserBeanLazy;

    //加装初始化实例跟Lazy相同,只不过没次调用都会生成一个新的对象(未使用Singleton修饰，当修饰了则每次都会调用的是同一个对象)
    @Inject
    Provider<UserDataBean> mUserDataBeanProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dagger_main);
        DaggerDaggerMainComponent.builder()
                .daggerUserModel(new DaggerUserModel())
                .build()
                .inject(this);
        showMess = findViewById(R.id.showMess);
        showMess.setText(dUser.showMess());
    }

    public void showMessClick(View view) {
        showMess.setText(dUser.showMess2());
    }

    public void onLoginCheck(View view) {
        startActivity(new Intent(this, LoginActivity.class));
    }

    public void onLoginCheck2(View view) {
        showMess.setText("1111111111111111");
    }

}
