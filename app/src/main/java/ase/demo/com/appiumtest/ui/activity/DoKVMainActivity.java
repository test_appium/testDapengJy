package ase.demo.com.appiumtest.ui.activity;

import android.app.Application;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import ase.demo.com.appiumtest.R;
import ase.demo.com.appiumtest.bean.UserBean;
import ase.demo.com.appiumtest.bean.UserBeanDoKV;
import leavesc.hello.dokv.DoKV;
import leavesc.hello.dokv_imp.MMKVDoKVHolder;

public class DoKVMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_do_kvmain);
        DoKV.init(new MMKVDoKVHolder(this));

        UserBean userBean =new UserBean();
        userBean.setUserRole("使用DoKV做存储");
        userBean.setOpenStatu("刚存进来的");
        userBean.setTrialCourse("这是描述");
        UserBeanDoKV.get().setUserBean(userBean);



    }

    public void onOpenUi(View view){
        startActivity(new Intent(DoKVMainActivity.this,DoKVMainNewActivity.class));

    }
}
