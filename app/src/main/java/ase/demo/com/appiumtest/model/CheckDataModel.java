package ase.demo.com.appiumtest.model;

import javax.inject.Singleton;

import ase.demo.com.appiumtest.bean.CheckUserBean;
import dagger.Module;
import dagger.Provides;

@Module
public class CheckDataModel {

    public CheckDataModel() {
    }

    @Singleton
    @Provides
    public CheckUserBean getCub() {
        return new CheckUserBean();
    }
}
