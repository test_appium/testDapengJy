package ase.demo.com.appiumtest.IPresenter;

public interface IPresenterBack {

    public interface Callback {
        public void finish(Object object);

        public void error(String mess);
    }

    public interface Presenter {
    }


}
