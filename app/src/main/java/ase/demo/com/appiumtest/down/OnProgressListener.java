package ase.demo.com.appiumtest.down;

import java.io.File;

//下载进度接口
public interface OnProgressListener {

    void updateProgress(int max, int progress);

    void onSuccess(File file);
}
