package ase.demo.com.appiumtest.ui.activity;

import android.content.res.Configuration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import ase.demo.com.appiumtest.util.LD;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.http.HttpHeaders;

public class InterceptorOkHttp implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
//        // 拦截请求，获取到该次请求的request
//        Request request = chain.request();
//        // 执行本次网络请求操作，返回response信息
//        Response response = chain.proceed(request);
//        if (true) {
//            for (String key : request.headers().toMultimap().keySet()) {
//                LD.W("header: {" + key + " : " + request.headers().toMultimap().get(key) + "}");
//            }
//            LD.W("url: " + request.url().uri().toString());
//            ResponseBody responseBody = response.body();
//            /*if (HttpHeaders.hasBody(response) && responseBody != null) {
//                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(responseBody.byteStream(), "utf-8"));
//                String result;
//                while ((result = bufferedReader.readLine()) != null) {
//                    LD.W("response: " + result);
//                }
//                // 测试代码
//                responseBody.string();
//            }*/
//
//        }
//        // 注意，这样写，等于重新创建Request，获取新的Response，避免在执行以上代码时，
//        // 调用了responseBody.string()而不能在返回体中再次调用
//        return response;//.newBuilder().build();


        Request request1 = chain.request();
        long t1 = System.nanoTime();
        LD.W(String.format("Sending request %s on %s%n%s",
                request1.url(), chain.connection(), request1.headers()));

        //官方文档中，少了下面的代码
        Request request = new Request.Builder()
                .url("https://publicobject.com/helloworld.txt")
                .build();

        Response response = chain.proceed(request);

        long t2 = System.nanoTime();
        LD.V(String.format("Received response for %s in %.1fms%n%s",
                response.request().url(), (t2 - t1) / 1e6d, response.headers()));

        return response;
    }
}
