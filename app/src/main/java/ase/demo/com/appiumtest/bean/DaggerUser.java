package ase.demo.com.appiumtest.bean;

import javax.inject.Inject;

import ase.demo.com.appiumtest.util.LD;

public class DaggerUser {
    String name;
    int age;
    String address;

    public DaggerUser() {
        LD.W("new DaggerUser()");
    }

    public String showMess() {
        return "张三 18岁 北京朝阳区";
    }

    public String showMess2() {
        return "张三三 15岁 山东淄博";
    }
}
