package ase.demo.com.appiumtest.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import ase.demo.com.appiumtest.R;
import ase.demo.com.appiumtest.bean.UserBean;
import ase.demo.com.appiumtest.bean.UserBeanDoKV;
import leavesc.hello.dokv.DoKV;
import leavesc.hello.dokv_imp.MMKVDoKVHolder;

public class DoKVMainNewActivity extends AppCompatActivity {

    TextView showContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_do_kvmain);
        showContent = findViewById(R.id.showContent);


        UserBean userBean = UserBeanDoKV.get().getUserBean();
        showContent.setText(userBean.getOpenStatu() + "\n" + userBean.getUserRole() + "\n" + userBean.getTrialCourse());


    }


}
